function [t,y] = euler( f, y0, t0, tN, N )
%explicit euler method for arbitrary IVP of first order y' = f(t,y)

%checking input for validity
if t0 > tN || N <= 0
    error('check input parameters: t0 > tN or N <= 0')
end

%creating mesh
t = linspace(t0,tN,N+1);
h = (tN-t0)/N;

%number of equations is given by size of initial value vector
[a,b] = size(y0);
c = max(a,b);


%make inital value vector a column
if a < b
    y0 = y0';   
end

%finding dimensions of solution matrix
[n,m] = size(t);

%initialising solution matrix
y = zeros(c,m);
y(:,1) = y0;

%euler iteration
for i = 2:m
    y(:,i) = y(:, i-1) + h.*f(t(i-1), y(:,i-1));
end

%plot(t, y(1,:));
%hold
%plot(t, y(2,:));

end

