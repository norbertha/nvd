function [u, y] = femtest(n, N, meshtype)
%meshtype can be 'uniform' or 'random'
%n is one which of the 5 test problems we try
%N influences the mesh size

%parameter checking

%test cases
switch n
    
  case 1
 
    %test problem 1
    f = @(x) 1;
    a = 0;
    b = 1;
    alpha = 1;
    beta = 0;
    gamma = 0;
    ua = 0;
    ub = 0;
    
    %mesh
    intervals = 2^N;
    if strcmp(meshtype,'uniform')
        nodes = a:1/intervals:b;
    elseif strcmp(meshtype,'random')
        r = a + (b-a).*rand(intervals-1,1);
        nodes(2:intervals) = sort(r);
        nodes(1) = a;
        nodes (intervals+1) = b;
    else
        error('unknown mesh type - chose uniform or random')  
    end
    
    %analytic
    u = ((-1/2).*(nodes-1).*nodes)';
    figure('Name', 'analytic')
    plot(nodes, u, 'r');
    
    %fem
    y = fem(nodes, f, ua, ub, alpha, beta, gamma);
    figure('Name', 'fem')
    plot(nodes, y, 'b');
    
  case 2
    
    f = @(x) 1;
    a = 0;
    b = 2;
    alpha = 1;
    beta = 0;
    gamma = 0;
    ua = 0;
    ub = 1;
    
    %mesh
    intervals = 2^N;
    if strcmp(meshtype,'uniform')
        nodes = a:1/intervals:b;
    elseif strcmp(meshtype,'random')
        r = a + (b-a).*rand(intervals-1,1);
        nodes(2:intervals) = sort(r);
        nodes(1) = a;
        nodes (intervals+1) = b;
    else
        error('unknown mesh type - chose uniform or random')  
    end
    
    %analytic
    u = ((-1/2).*(nodes-3).*nodes)';
    figure('Name', 'analytic')
    plot(nodes, u, 'r');
    
    %fem
    y = fem(nodes, f, ua, ub, alpha, beta, gamma);
    figure('Name', 'fem')
    plot(nodes, y, 'b');
    
  case 3  
    
    f = @(x) x;
    a = 0;
    b = 1;
    alpha = 1;
    beta = 0;
    gamma = 0;
    ua = 0;
    ub = 0;
    
    %mesh
    intervals = 2^N;
    if strcmp(meshtype,'uniform')
        nodes = a:1/intervals:b;
    elseif strcmp(meshtype,'random')
        r = a + (b-a).*rand(intervals-1,1);
        nodes(2:intervals) = sort(r);
        nodes(1) = a;
        nodes (intervals+1) = b;
    else
        error('unknown mesh type - chose uniform or random')  
    end
    
    %analytic
    u = ((1/6).*(nodes-nodes.^3))';
    figure('Name', 'analytic')
    plot(nodes, u, 'r');
    
    %fem
    y = fem(nodes, f, ua, ub, alpha, beta, gamma);
    figure('Name', 'fem')
    plot(nodes, y, 'b');
    
  case 4
    
    f = @(x) 1;
    a = 0;
    b = 1;
    alpha = 1;
    beta = 0;
    gamma = 1/100;
    ua = 0;
    ub = 0;
    
   %mesh
    intervals = 2^N;
    if strcmp(meshtype,'uniform')
        nodes = a:1/intervals:b;
    elseif strcmp(meshtype,'random')
        r = a + (b-a).*rand(intervals-1,1);
        nodes(2:intervals) = sort(r);
        nodes(1) = a;
        nodes (intervals+1) = b;
    else
        error('unknown mesh type - chose uniform or random')  
    end
    
    %analytic
    u = ((100*(-exp((1-nodes)./10)-exp(nodes./10)+1+exp(1/10)))/(1+exp(1/10)))';
    figure('Name', 'analytic')
    plot(nodes, u, 'r');
    
    %fem
    y = fem(nodes, f, ua, ub, alpha, beta, gamma);
    figure('Name', 'fem')
    plot(nodes, y, 'b');
  
  case 5
  
    f = @(x) 1;
    a = 0;
    b = 1;
    alpha = 1;
    beta = 1/10;
    gamma = 0;
    ua = 0;
    ub = 0;
    
    %mesh
    intervals = 2^N;
    if strcmp(meshtype,'uniform')
        nodes = a:1/intervals:b;
    elseif strcmp(meshtype,'random')
        r = a + (b-a).*rand(intervals-1,1);
        nodes(2:intervals) = sort(r);
        nodes(1) = a;
        nodes (intervals+1) = b;
    else
        error('unknown mesh type - chose uniform or random')  
    end
    
    %analytic
    u = ((-10*(-exp(1/10).*nodes+nodes+exp(nodes./10)-1))/(exp(1/10)-1))';
    figure('Name', 'analytic')
    plot(nodes, u, 'r');
    
    %fem
    y = fem(nodes, f, ua, ub, alpha, beta, gamma);
    figure('Name', 'fem')
    plot(nodes, y, 'b');
  
end


end
