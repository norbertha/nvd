function [ solution ] = fem( mesh, f, ua, ub, alpha, beta, gamma )
%Finite Element Method for model problem
%
%   -alpha*u''(x) + beta*u'(x) + gamma*u(x) = f(x) for x in (a,b)
%    u(a) = ua
%    u(b) = ub
%
%the mesh doesn't need to be uniform;
%alpha is supposed to be bigger than 0; alpha, beta, gamma beeing constants;

%mesh column
[x,y] = size(mesh);
if x < y
    mesh = mesh';
end

%meshsizes
h = zeros(length(mesh)-1, 1);
for i = 1:length(h)
    h(i) = mesh(i+1)-mesh(i);
end


%right hand side of our system of linear equations with Simpson quadrature
b = zeros(length(mesh)-2, 1);
for i = 2:(length(b)+1)
    b(i-1) = (1/6)*( 4*f( (mesh(i-1)+mesh(i))/2 ) * h(i-1)/2 + f(mesh(i))*h(i-1) + f(mesh(i))*h(i) + 4*f( (mesh(i)+mesh(i+1))/2 ) * h(i)/2 );
end

%boundary condition lifting
b(1) = b(1) - ua*( -alpha/h(1) - beta/2 + gamma * h(1)/6);
dimb = length(b);
b(dimb) = b(dimb) - ub*( -alpha/h(dimb+1) + beta/2 + gamma * h(dimb+1)/6);

%toeplitz matrix depending on coefficients alpha, beta, gamma AND meshsizes
%d = diagonal elements; 
%r = diagonal-1;
%s = diagonal+1;
s = (-alpha./h + beta/2+(gamma.*h)/6);
d = zeros(length(mesh)-2,1);
r = (-alpha./h - beta/2+(gamma.*h)/6);

for i = 2:length(mesh)-1
    d(i-1) = alpha*(1/h(i-1)+1/h(i))+gamma/3*(h(i-1)+h(i));
end
    
A = diag(d) + diag(r(2:length(r)-1), -1) +  diag(s(2:length(s)-1), 1);

%solving 
sol = A\b;

%discrete solution
solution = [ua; sol; ub];


end

