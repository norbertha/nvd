%consider the following problem and a uniform mesh
%
%   -epsilon*u''(x) + beta*u'(x) = 1  for x in (0,1)
%    u(0) = 0;
%    u(1) = 0;
%

%problem
f = @(x) 1;
a = 0; b = 1;
ua = 0; ub = 0;
epsilon = 0.01;
beta = -1;
gamma = 0;

%mesh
nodes = cell(3,1,1);
%analytic solution
u = cell(3,1,1);
%fem solution without ariticial diffusion
y = cell(3,1,1);

figure('Name', 'comparison');
for i = 1:3
    
    nodes{i} = a:2*10^(-i):b;
    u{i} = -nodes{i}-exp(-100*nodes{i})+1;
    y{i} = fem( nodes{i}, f, ua, ub, epsilon, beta, gamma);
    
    subplot(2,3,i);
    plot(nodes{i},u{i});
    title(['analytic h=', num2str(2*10^(-i))]);
    
    subplot(2,3,i+3);
    plot(nodes{i},y{i});
    title(['FEM h=', num2str(2*10^(-i))]);
    
end

%h = 0.2 with artificial diffusion
adepsilon = epsilon + beta*0.2/2;
z = fem(nodes{1}, f, ua, ub, adepsilon, beta, gamma);

figure('Name', 'artificial diffusion')

subplot(1,3,1);
plot(nodes{1}, z);
title('with ad')

subplot(1,3,2);
plot(nodes{1}, y{1});
title('without ad')

subplot(1,3,3);
plot(nodes{3}, u{3});
title('analytic')
