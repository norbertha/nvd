%exercise 2
f = @(x) x; a = 0; b = 2; ua = 1; ub = 2;
uanalytic = @(x) -x^3/6+7/6*x+1;
N = 10*2.^(0:1:5);

mesh = cell(6,1,1);
uapproximation = cell(6,1,1);
usolution = cell(6,1,1);
error = zeros(6,1);
convrate = zeros(5,1);

for i = 1:6
    mesh{i} = a:(b-a)/N(i):b;
    uapproximation{i} = fdm1(mesh{i}, f, ua, ub);
    usolution{i} = arrayfun(uanalytic, mesh{i})';
    error(i) = norm(usolution{i}-uapproximation{i}, Inf);
end

for j = 1:5
    convrate(j) = log(error(j)/error(j+1))/log(2);
end

loglog(N,error)
figure
plot(N(2:6), convrate)
figure
plot(mesh{6}, usolution{6})
figure
plot(mesh{6}, uapproximation{6}, 'r')