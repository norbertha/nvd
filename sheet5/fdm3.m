function [ solution ] = fdm3( mesh, f, ua, ub )
%Finite Difference Method (FDM) for one dimensional Poisson problem with inhomogenous Dirichlet boundary conditions
%-u''(x) + 2*u'(x) + 3*u(x) = f(x) for x in (a,b)
%u(a) = ua, u(b) = ub
%solution = fofmesh\A
%testproblem: f = @(x) 1; a = 0; b = 1; ua = 0; ub = 0; mesh = a:0.1:b;


%mesh is supposed to be a column vector
[x,y] = size(mesh);

if x < y
    mesh = mesh';
end

%supposedly equidistant mesh
h = mesh(2)-mesh(1);

%interior grid points
%f evaluated at interior grid points
newmesh = mesh(2:length(mesh)-1);
fofmesh = arrayfun(f, newmesh);

%matrix A
A = 1/h^2.*toeplitz([2+3*h^2 -1-h zeros(1, length(newmesh)-2)], [2+3*h^2 -1+h zeros(1, length(newmesh)-2)]);

%solving the system of linear equations
sol = A\fofmesh;

%lifting of boundary conditions
ulift = (ub-ua)/(mesh(length(mesh))-mesh(1)).*newmesh + ua - (ub-ua)/(mesh(length(mesh))-mesh(1))*mesh(1);
sol = sol + ulift;

%full solution
solution = [ua; sol; ub];


end

