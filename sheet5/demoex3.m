%exercise 3
f = @(x) 1; a = 0; b = 1; ua = 0; ub = 0;
uanalytic = @(x) (1/3)*(1+(1-exp(-1))/(exp(-1)-exp(3))*exp(3*x)+(exp(3)-1)/(exp(-1)-exp(3))*exp(-x));
N = 10*2.^(0:1:5);

mesh = cell(6,1,1);
uapproximation = cell(6,1,1);
usolution = cell(6,1,1);
error = zeros(6,1);
convrate = zeros(5,1);

for i = 1:6
    mesh{i} = a:(b-a)/N(i):b;
    uapproximation{i} = fdm3(mesh{i}, f, ua, ub);
    usolution{i} = arrayfun(uanalytic, mesh{i})';
    error(i) = norm(usolution{i}-uapproximation{i}, Inf);
end

for j = 1:5
    convrate(j) = log(error(j)/error(j+1))/log(2);
end

loglog(N,error)
figure
plot(N(2:6), convrate)
figure
plot(mesh{6}, usolution{6})
figure
plot(mesh{6}, uapproximation{6}, 'r')