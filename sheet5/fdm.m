function [ solution ] = fmd(  mesh, f, ua, ub, alpha, beta, gamma )
%Upwind Finite Differences for model problem
%
%   -alpha*u''(x) + beta*u'(x) + gamma*u(x) = f(x) for x in (a,b)
%    u(a) = ua
%    u(b) = ub
%
%the mesh is supposed to be uniform;
%alpha is supposed to be bigger than 0; alpha,beta,gamma constants

%mesh is supposed to be a column vector
[x,y] = size(mesh);
if x < y
    mesh = mesh';
end

%meshsize
h = mesh(2)-mesh(1);

%f evaluated on interior gridpoints
newmesh = mesh(2:length(mesh)-1);
fofmesh = arrayfun(f, newmesh);

%toeplitzmatrix depending on coefficients alpha, beta, gamma
%diagonal element:  2*alpha + h*abs(beta) + h^2*gamma
%diagonal +1:       -alpha - h*max(beta,0)
%diagonal -1:       -alpha + h*min(beta,0)
A = h^(-2).*toeplitz([2*alpha+h*abs(beta)+h^2*gamma -alpha-h*max(beta,0) zeros(1, length(newmesh)-2)], [2*alpha+h*abs(beta)+h^2*gamma -alpha+h*min(beta,0) zeros(1, length(newmesh)-2)]);

%solving 
sol = A\fofmesh;

%lifting of boundary conditions
ulift = (ub-ua)/(mesh(length(mesh))-mesh(1)).*newmesh + ua - (ub-ua)/(mesh(length(mesh))-mesh(1))*mesh(1);
sol = sol + ulift;

%assembling the solution
solution = [ua; sol; ub];


end

