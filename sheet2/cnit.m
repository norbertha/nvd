function [t,y] = cnit( f, y0, t0, tN, crit1, crit2, meshsize )
%Crank-Nicolson method with fix point iteration for solving arbitrary IVP systems of first order
%fix point iteration stops after either a number of steps are completed or the relative difference between iterations is too small
%crit1 = maximal steps of fix point iteration
%crit2 = minimal relative difference within the iterated values
%test function f:   f = @(t,y) [-y(1)-y(1)*y(2)*y(2)+294*y(2); (y(1)-y(1)*y(2))/98-3*y(2)]
%                   g = @(t,y) [-y(1)+0.1*y(1)*y(2); 4*y(2)-y(1)*y(2)]



%mesh
h = (tN-t0)/meshsize;
t = linspace(t0,tN,meshsize+1);
[n,m] = size(t);

%number of equations
[a,b] = size(y0);
c = max(a,b);

%working with columns
if a < b
    y0 = y0';
end

%initialising solution vector
%each column contains solutions for one point in the mesh
y = zeros(c,m);
y(:,1) = y0;

%initialising cn-method
for i = 2:m
    x0 = y(:,i-1);

    %initialising fix point iteration
    for j = 1:crit1
        x1 = y(:,i-1) + (h/2) * (f(t(i-1),y(:,i-1)) + f(t(i),x0));
     
        if norm(x1-x0)/j < crit2
            y(:,i-1) = x1;
            break
        end
        
        x0 = x1;

    end
    
    y(:,i) = y(:,i-1) + (h/2) * (f(t(i-1),y(:,i-1)) + f(t(i),x0)); 
    
end

%semilogy(t, y)
%hold


end

