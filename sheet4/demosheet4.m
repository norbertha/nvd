%problem from 1
f = @(t,y) -2*t*y.^2;
yexact = @(t) 1./(1+t.^2);
y0 = 1;
t0 = 0;
yatone = 1/2;

%number of steps  to reach t=1 with step size = 1/N
N = 10*2.^(0:1:5);

%calculating error and convergencerate for step sizes 1/N; N from {10, 20, 40, 80, 160, 320}
err = @(solution, N) abs(solution(N)-yatone);

errorexactatone = zeros(1,6);
erroreuleratone = zeros(1,6);
errorrk4atone = zeros(1,6);

convrateexact = zeros(1,5);
convrateeuler = zeros(1,5);
convraterk4 = zeros(1,5);

for i = 1:6
    
    [t,b] = ab4(f, y0, t0, N(i), 1/N(i), 'exact', yexact); 
    errorexactatone(i) = err(b, length(b));
    
    [t,d] = ab4(f, y0, t0, N(i), 1/N(i), 'euler'); 
    erroreuleratone(i) = err(d, length(d));
    
    [t,j] = ab4(f, y0, t0, N(i), 1/N(i), 'rk4'); 
    errorrk4atone(i) = err(j, length(j));
    
end

for j = 1:5
    convrateexact(j) = log(errorexactatone(j)/errorexactatone(j+1))/log(2);
    convrateeuler(j) = log(erroreuleratone(j)/erroreuleratone(j+1))/log(2);
    convraterk4(j) = log(errorrk4atone(j)/errorrk4atone(j+1))/log(2);
end


%compare solutions
t = (0:0.1:1);
y = yexact(t);
stepsz = 0.1;

[t1,y1] = ab4(f, y0, t0, 1/stepsz, stepsz, 'exact', yexact);
[t2,y2] = ab4(f, y0, t0, 1/stepsz, stepsz, 'euler');
[t3,y3] = ab4(f, y0, t0, 1/stepsz, stepsz, 'rk4');
title = sprintf('Comparing solutions at step size %f', stepsz); 

figure('Name', title)
plot(t,y,'r', t1,y1,'g', t2,y2,'b', t3,y3,'y');
legend( 'analytic solution', 'ab4/exact', 'ab4/euler', 'ab4/rk4');

%compare errors at t=1
figure('Name','Error at t=1')
loglog(N, errorexactatone, 'r', N, erroreuleratone, 'g', N, errorrk4atone, 'b')
legend( 'error of ab4/exakt', 'error of ab4/euler', 'error of ab4/rk4')

%comparing convergencerates
figure('Name','Convergence rates')
plot(N, convrateexact, 'r', N, convrateeuler, 'g', N, convraterk4, 'b')
legend( 'ab4/exakt', 'ab4/euler', 'ab4/rk4')



