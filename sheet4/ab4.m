function [ t, y ] = ab4( f, y0, t0, N, h, method, yexact )
%f:         system of odes; test: f = @(t,y)-2*t*y.^2; yexakt = @(t,y)1/(1+t^2);
%y0:        initale value for system
%t0:        start of the intervall
%N:         number of steps
%h:         step size
%method:   ( exact / rk4 / euler ) method for initialising
%yexact:    exact solution function if known and in case we initialise with exact values


t = (t0 : h : t0+N*h);
y = zeros(length(y0), N+1); 
y(:,1) = y0;


%argcontrol
if strcmp(method, 'exact') == 1 && nargin == 6
    error('Use different initialisation method or add exact solution as parameter!');    
else
    %initialising first 4 values
    switch method
        case 'rk4'
            [a,b] = rk4(f, y0, t0, 3, h);
            y(:,1:4) = b;
        case 'euler'
            [a,b] = feuler(f, y0, t0, 3, h);
            y(:,1:4) = b;
        case 'exact'
            for i = 2:4
                y(:,i) = yexact(t(i));
            end
        otherwise
            error('Unknown method!')
    end
end

%adams-bashforth
for j = 5 : N+1
    y(:,j) = y(:,j-1)+ (h/24)* (55*f(t(j-1),y(:,j-1)) - 59*f(t(j-2),y(:,j-2)) + 37*f(t(j-3),y(:,j-3)) -9*f(t(j-4), y(:,j-4)));
end


end

