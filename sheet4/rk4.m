function [ t, y ] = rk4( f, y0, t0, N, h )
%rk4 for system of odes
%f:     system of odes
%y0:    inital values
%t0:    start of intervall
%N:     number of steps
%h:     step size

t = (t0 : h : t0+N*h);
y = zeros(length(y0), N+1); 
y(:,1) = y0;

for i = 2:N+1
    
    k1 = f(t(i-1), y(:,i-1));
    k2 = f(t(i-1)+h/2, y(:,i-1)+h/2*k1);
    k3 = f(t(i-1)+h/2, y(:,i-1)+h/2*k2);
    k4 = f(t(i-1)+h, y(:,i-1)+h*k3);
    y(i) = y(i-1) + h/6*(k1+2*k2+2*k3+k4);
    
end