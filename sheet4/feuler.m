function [ t, y ] = feuler( f, y0, t0, N, h )
%forward euler for system of odes
%f:     system of odes
%y0:    inital values
%t0:    start of intervall
%N:     number of steps
%h:     step size

t = (t0 : h : t0+N*h);
y = zeros(length(y0), N+1); 
y(:,1) = y0;

for i = 2:N+1
    
    y(:,i) = y(:,i-1) + h*f(t(i-1),y(:,i-1));
    
end

end

